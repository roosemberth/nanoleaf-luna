{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Luna.Etc where

import Data.FileEmbed
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)

etcSpecFile :: Text
etcSpecFile = decodeUtf8 $(embedFile "res/etc-spec.yaml")
