{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Main where

import Control.Concurrent (threadDelay)
import Control.Monad.Reader
import Data.Aeson
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types (parseEither, parseMaybe)
import Data.Char (toLower)
import Data.Functor ((<&>))
import Data.Hashable (Hashable)
import Data.Maybe (catMaybes, fromMaybe, isJust)
import Data.Proxy
import Data.Text (Text)
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import GHC.TypeLits (KnownNat)
import GHC.TypeNats
import Luna.Etc
import Luna.LeafClient
import Luna.LeafCustomEffects
import Servant.Client (ClientError, BaseUrl(..), Scheme(..))
import System.Environment (lookupEnv)
import System.Exit (die)
import Test.QuickCheck (Gen(..), Arbitrary(..), generate)

import qualified Data.Text                      as T
import qualified System.Etc                     as Etc
import qualified System.Etc.Internal.Spec.Types as Etc

data Cmd = DebugConfig | Demo | Off | StaticColor
  deriving (Show, Eq, Generic, Hashable)

$(deriveJSON defaultOptions{ constructorTagModifier = map toLower } ''Cmd)

data ConnectionInfo = ConnectionInfo
  { ip :: Text
  , port :: Int
  , token :: Text
  } deriving (Show, Eq, Generic, FromJSON, ToJSON)

mkNPaneFrames :: PanelId -> Int -> Gen PaneFrames
mkNPaneFrames (someNatVal -> SomeNat (p :: Proxy pid)) n =
    PaneFrames <$> replicateM n (arbitrary @(PaneFrame pid))

randomAnimations :: [PanelId] -> Gen PaneAnimation
randomAnimations ids = do
  lengths :: [Int] <- replicateM (length ids) arbitrary
  PaneAnimation <$> zipWithM mkNPaneFrames ids lengths

runAPICall :: ConnectionInfo -> AuthClientM a -> IO (Either ClientError a)
runAPICall ConnectionInfo{..} op =
  runReaderT (unwrapAuthClient op) (BaseUrl Http (T.unpack ip) port "", token)

runDemo :: ConnectionInfo -> IO ()
runDemo c = do
  panelIds <- runAPICall c getPanelsLayout >>=
                either (fail . show) (pure . fmap panelId . positionData)

  putStrLn $ "Nanoleaf has panels with IDs: " <> show panelIds
  putStrLn "Generating random patterns :D"

  generate (randomAnimations panelIds) >>=
    void . runAPICall c . setEffect . showPaneAnimationCmd

runStaticColor :: (Int, Int, Int) -> ConnectionInfo -> IO ()
runStaticColor (r, g, b) c = do
  panelIds <- runAPICall c getPanelsLayout >>=
                either (fail . show) (pure . fmap panelId . positionData)
  putStrLn $ "Setting all panes to " <> show (r, g, b)
  let mkPaneFrames (someNatVal -> SomeNat (p :: Proxy pid)) =
        PaneFrames [PaneFrame r g b 10 :: PaneFrame pid]
  void . runAPICall c . setEffect . showPaneAnimationCmd $
    PaneAnimation (map mkPaneFrames panelIds)

guardConnConfig :: Value -> IO ConnectionInfo
guardConnConfig obj@(Object cfg) = do
    let failMissingProp name = die $ name <> " not found in config file\
          \ nor specified in the command line arguments."
        hasProp prop = case parseMaybe @_ @Value (cfg .:) prop of
          Nothing -> False
          Just Null -> False
          Just _ -> True

    unless (hasProp "ip") $ failMissingProp "Nanoleaf API IP address"
    unless (hasProp "token") $ failMissingProp "Nanoleaf API token"
    either fail pure $ parseEither parseJSON obj

genEtcFileSpec :: IO Etc.FilesSpec
genEtcFileSpec =
  Etc.FilesSpec (Just "NANOLEAF_LUNA_CONFIG") . fmap T.pack . catMaybes
    <$> sequence
      [ pure $ Just "/etc/nanoleaf-luna/config.yaml"
      , envSubPath "HOME" "/.nanoleaf-luna.yaml"
      , envSubPath "XDG_CONFIG_HOME" "/nanoleaf-luna/config.yaml"
      , pure $ Just "./nanoleaf-luna.yaml"
      ]
  where envSubPath envvar subpath = lookupEnv envvar <&> fmap (<> subpath)

main :: IO ()
main = do
  spec <- (\spec fSpec -> spec { Etc.specConfigFilepaths = Just fSpec })
            <$> Etc.parseConfigSpec etcSpecFile <*> genEtcFileSpec
  let defConfig = Etc.resolveDefault spec
  envConfig <- Etc.resolveEnv spec
  (fConfig, _) <- Etc.resolveFiles spec
  (cmd :: Cmd, cliConfig) <- Etc.resolveCommandCli spec

  let etcCfg = mconcat [defConfig, fConfig, cliConfig, envConfig]

  case cmd of
    DebugConfig -> print etcCfg
    other -> Etc.getConfigValue ["auth"] etcCfg
      >>= guardConnConfig
      >>= \cfg -> case other of
        Demo -> runDemo cfg
        Off -> void $ runAPICall cfg
          (setState . LeafSetPowerState . LeafBinaryState $ False)
        StaticColor -> do
          color <- (\[a, b, c] -> (a, b, c)) <$> sequence
            [ Etc.getConfigValue ["staticcolor", "R"] etcCfg
            , Etc.getConfigValue ["staticcolor", "G"] etcCfg
            , Etc.getConfigValue ["staticcolor", "B"] etcCfg
            ]
          runStaticColor color cfg
