{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Luna.LeafClient where

import Control.Applicative
import Control.Monad.Reader
import Data.Aeson
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types (Parser, typeMismatch)
import Data.Proxy
import GHC.Generics
import Luna.Util
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.API
import Servant.Client

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Numeric.Natural (Natural)

type LeafURL = BaseUrl

data NewAuth = NewAuth { auth_token :: T.Text }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

-- | This API should be mounted under `/`
type UnauthenticatedAPI = "api" :> "v1" :> "new" :> Post '[JSON] NewAuth

newtype UnAuthClientM a = UnAuthClientM
  { unwrapUnAuthClient :: ReaderT LeafURL IO (Either ClientError a) }

getNewAuth :: UnAuthClientM NewAuth

-- | UnAuthenticated APIs
getNewAuth = hoistClient api nt (client api)
  where api = Proxy @UnauthenticatedAPI
        nt ep = UnAuthClientM $ do
          leafUrl <- ask
          let baseUrl = leafUrl { baseUrlPath = path }
              path = baseUrlPath leafUrl ++ "/api/v1"
          clientMgr <- liftIO $ newManager defaultManagerSettings
          liftIO $ runClientM ep (mkClientEnv clientMgr baseUrl)

data LeafBinaryState = LeafBinaryState { value :: Bool }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafGenericColorState = LeafGenericColorState
  { value, increment, min, max :: Maybe Int }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafSetState =
    LeafSetPowerState      { on         :: LeafBinaryState }
  | LeafSetBrightnessState { brightness :: LeafGenericColorState }
  | LeafSetHueState        { hue        :: LeafGenericColorState }
  | LeafSetSaturationState { sat        :: LeafGenericColorState }
  | LeafSetColorTempState  { ct         :: LeafGenericColorState }
  deriving (Show, Eq)

$(deriveJSON defaultOptions{ sumEncoding = UntaggedValue } ''LeafSetState)

data LeafEffectCommand =
    LeafEffectCommandAdd
  | LeafEffectCommandDelete
  | LeafEffectCommandRequest
  | LeafEffectCommandDisplay
  | LeafEffectCommandRename
  | LeafEffectCommandRequestAll
  | LeafEffectCommandDisplayTemp -- | ^ Undocumented
  | LeafEffectCommandRequestPlugins -- | ^ Undocumented
  deriving (Show, Eq, Generic)

$(deriveJSON
  defaultOptions{ constructorTagModifier = lowerHead . drop 17 }
  ''LeafEffectCommand)

data LeafEffectAnimType =
    LeafEffectAnimTypeRandom
  | LeafEffectAnimTypeFlow
  | LeafEffectAnimTypeWheel
  | LeafEffectAnimTypeFade
  | LeafEffectAnimTypeHighlight
  | LeafEffectAnimTypeCustom
  | LeafEffectAnimTypeStatic
  deriving (Show, Eq, Generic)

$(deriveJSON
  defaultOptions{ constructorTagModifier = lowerHead . drop 18 }
  ''LeafEffectAnimType)

-- | This type is ill-defined in the API
data LeafEffectPalette = LeafEffectPalette
  { hue :: Maybe Int
  , saturation :: Maybe Int
  , brightness :: Maybe Int
  , probability :: Maybe Int
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

-- | This type is ill-defined in the API
data LeafEffectGenericRange = LeafEffectGenericRange
  { minValue :: Maybe Double
  , maxValue :: Maybe Double
  } deriving (Show, Read, Eq, Generic, ToJSON, FromJSON)

newtype LeafEffectValueOrRange =
  LeafEffectValueOrRange (Either LeafEffectGenericRange Double)
  deriving Eq

instance Show LeafEffectValueOrRange where
  show (LeafEffectValueOrRange (Right a)) = show a
  show (LeafEffectValueOrRange (Left a)) = show a

instance Read LeafEffectValueOrRange where
  readsPrec n value = mapL LeafEffectValueOrRange <$> if null pAsRange
    then mapL Right <$> readsPrec @Double n value
    else mapL Left <$> pAsRange
    where pAsRange = readsPrec @LeafEffectGenericRange n value
          mapL f (a, b) = (f a, b)

instance ToJSON LeafEffectValueOrRange where
  toJSON (LeafEffectValueOrRange (Right a)) = toJSON a
  toJSON (LeafEffectValueOrRange (Left a)) = toJSON a

instance FromJSON LeafEffectValueOrRange where
  parseJSON n@(Number _) = LeafEffectValueOrRange . Right
    <$> parseJSON @Double n
  parseJSON (Object o) = LeafEffectValueOrRange . Left
    <$> (LeafEffectGenericRange <$> o .: "minValue" <*> o .: "maxValue")
  parseJSON v = typeMismatch "Range or Value" v

-- | LeafEffectCmdObj is a bizarre type implementing an extensible protocol
-- for managing 'plugins' using a single object.
data LeafEffectCmdObj = LeafEffectCmd
  { command :: Maybe LeafEffectCommand
  , version :: Maybe T.Text
  , duration :: Maybe Double
  , animName :: Maybe T.Text
  , newName :: Maybe T.Text
  , animType :: Maybe LeafEffectAnimType
  , animData :: Maybe T.Text
  , colorType :: Maybe T.Text
  , palette :: Maybe [LeafEffectPalette]
  , brightnessRange :: Maybe LeafEffectGenericRange
  , transTime :: Maybe LeafEffectValueOrRange
  , delayTime :: Maybe LeafEffectValueOrRange
  , flowFactor :: Maybe Double
  , explodeFactor :: Maybe Double
  , windowSize :: Maybe Double
  , direction :: Maybe T.Text
  , loop :: Maybe Bool
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions{ omitNothingFields = True } ''LeafEffectCmdObj)

data LeafEffectSetState = LeafEffectSetState
  { select :: Maybe T.Text
  , write :: Maybe LeafEffectCmdObj
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions{ omitNothingFields = True } ''LeafEffectSetState)

data LeafOrientation = LeafOrientation { value :: Int, min, max :: Maybe Int }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafSetLayout = LeafSetLayout { globalOrientation :: LeafOrientation }
  deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafPanelShapeType =
    Triangle
  | Rhythm
  | Square
  | ControlSquarePrimary
  | ControlSquarePassive
  | PowerSupply
  deriving (Show, Eq)

instance FromJSON LeafPanelShapeType where
  parseJSON (Number 0) = pure Triangle
  parseJSON (Number 1) = pure Rhythm
  parseJSON (Number 2) = pure Square
  parseJSON (Number 3) = pure ControlSquarePrimary
  parseJSON (Number 4) = pure ControlSquarePassive
  parseJSON (Number 5) = pure PowerSupply
  parseJSON a = typeMismatch "Leaf panel shape enum" a

instance ToJSON LeafPanelShapeType where
  toJSON Triangle             = toJSON (Number 0)
  toJSON Rhythm               = toJSON (Number 1)
  toJSON Square               = toJSON (Number 2)
  toJSON ControlSquarePrimary = toJSON (Number 3)
  toJSON ControlSquarePassive = toJSON (Number 4)
  toJSON PowerSupply          = toJSON (Number 5)

type PanelId = Natural

data LeafPanelPositionData = LeafPanelPositionData
  { panelId :: PanelId
  , x :: Double
  , y :: Double
  , o :: Double
  , shapeType :: LeafPanelShapeType
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafLayout = LeafLayout
  { numPanels :: Int
  , sideLength :: Int
  , positionData :: [ LeafPanelPositionData ]
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafRhythmMode = RhythmModeMicrophone | RhythmModeAux
  deriving (Show, Eq)

instance FromJSON LeafRhythmMode where
    parseJSON (Number 0) = pure RhythmModeMicrophone
    parseJSON (Number 1) = pure RhythmModeAux
    parseJSON a = typeMismatch "Rhythm Mode enum" a

instance ToJSON LeafRhythmMode where
    toJSON RhythmModeMicrophone = toJSON (Number 0)
    toJSON RhythmModeAux = toJSON (Number 1)

data LeafRhythmPosition = LeafRhythmPosition
  { x :: Double
  , y :: Double
  , o :: Double
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafSetRhythm =
    LeafSetRhythmMode { rhythmMode :: LeafRhythmMode }
  deriving (Show, Eq)

$(deriveJSON defaultOptions{ sumEncoding = UntaggedValue } ''LeafSetRhythm)

type PanelEffect = T.Text

-- | Empirically-determined... Your mileage may vary...
data LeafPanelEffectState = LeafPanelEffectState
  { select :: PanelEffect
  , effectsList :: [PanelEffect]
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafPanelLayout = LeafPanelLayout
  { globalOrientation :: LeafOrientation
  , layout :: LeafLayout
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafRhythmInfo = LeafRhythmInfo
  { auxAvailable :: Bool
  , firmwareVersion :: T.Text
  , hardwareVersion :: T.Text
  , rhythmActive :: Bool
  , rhythmConnected :: Bool
  , rhythmId :: Int
  , rhythmPos :: LeafRhythmPosition
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

data LeafPanelState = LeafPanelState
  { brightness :: LeafGenericColorState
  , colorMode :: T.Text -- ?
  , ct :: LeafGenericColorState
  , hue :: LeafGenericColorState
  , on :: LeafBinaryState
  , sat :: LeafGenericColorState
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

-- | Empirically-determined... Your mileage may vary...
data PanelInfo = PanelInfo
  { name :: T.Text
  , serialNo :: T.Text
  , manufacturer :: T.Text
  , firmwareVersion :: T.Text
  , hardwareVersion :: T.Text
  , model :: T.Text
  , cloudHash :: Value -- ^ ??? My Nanoleaf returns an empty object
  , discovery :: Value -- ^ ??? My Nanoleaf returns an empty object
  , effects :: LeafPanelEffectState
  , firmwareUpgrade :: Value -- ^ ??? My Nanoleaf returns an empty object
  , panelLayout :: LeafPanelLayout
  , rhythm :: LeafRhythmInfo
  , schedules :: Value -- ^ ??? My Nanoleaf returns an empty object
  , state :: LeafPanelState
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

-- | This API should be mounted under `/api/v1/<token>/`
type AuthenticatedAPI =
       Get '[JSON] PanelInfo
  -- | Color-related APIs
  :<|> "state" :> "on"         :> Get '[JSON] LeafBinaryState
  :<|> "state" :> "brightness" :> Get '[JSON] LeafGenericColorState
  :<|> "state" :> "hue"        :> Get '[JSON] LeafGenericColorState
  :<|> "state" :> "saturation" :> Get '[JSON] LeafGenericColorState
  :<|> "state" :> "ct"         :> Get '[JSON] LeafGenericColorState
  :<|> "state" :> "colorMode"  :> Get '[JSON] T.Text -- ?
  :<|> "state" :> ReqBody '[JSON] LeafSetState :> PutNoContent '[NoContent] NoContent
  -- | Effects
  :<|> "effects" :> "select"  :> Get '[JSON] T.Text
  :<|> "effects" :> "effectsList"  :> Get '[JSON] [T.Text]
  :<|> "effects" :> ReqBody '[JSON] LeafEffectSetState :> PutNoContent '[NoContent] NoContent
  -- | Panel Layout APIs
  :<|> "panelLayout" :> "globalOrientation" :> Get '[JSON] LeafOrientation
  :<|> "panelLayout" :> "layout" :> Get '[JSON] LeafLayout
  :<|> "panelLayout" :> ReqBody '[JSON] LeafSetLayout :> PutNoContent '[NoContent] NoContent
  -- | Idenfity
  :<|> "identify" :> PutNoContent '[NoContent] NoContent
  -- | External Control (Streaming): See Effects
  -- | Rhythm-related APIs
  :<|> "rhythm" :> "rhythmConnected" :> Get '[JSON] LeafBinaryState
  :<|> "rhythm" :> "rhythmActive" :> Get '[JSON] LeafBinaryState
  :<|> "rhythm" :> "rhythmId" :> Get '[JSON] Int
  :<|> "rhythm" :> "hardwareVersion" :> Get '[JSON] T.Text
  :<|> "rhythm" :> "firmwareVersion" :> Get '[JSON] T.Text
  :<|> "rhythm" :> "auxAvailable" :> Get '[JSON] LeafBinaryState
  :<|> "rhythm" :> "rhythmMode" :> Get '[JSON] LeafRhythmMode
  :<|> "rhythm" :> "rhythmPos" :> Get '[JSON] LeafRhythmPosition
  :<|> "rhythm" :> ReqBody '[JSON] LeafSetRhythm :> PutNoContent '[OctetStream] NoContent

-- | Color-related APIs
getPanelInfo       :: AuthClientM PanelInfo
getPowerState      :: AuthClientM LeafBinaryState
getBrightnessState :: AuthClientM LeafGenericColorState
getHueState        :: AuthClientM LeafGenericColorState
getSaturationState :: AuthClientM LeafGenericColorState
getColorTempState  :: AuthClientM LeafGenericColorState
getColorModeState  :: AuthClientM T.Text
setState           :: LeafSetState -> AuthClientM NoContent

-- | Effects APIs
getSelectedEffect   :: AuthClientM T.Text
getAvailableEffects :: AuthClientM [T.Text]
setEffect           :: LeafEffectSetState -> AuthClientM NoContent

-- | Panel Layout APIs
getGlobalOrientation :: AuthClientM LeafOrientation
getPanelsLayout :: AuthClientM LeafLayout
setLayout :: LeafSetLayout -> AuthClientM NoContent

-- | Identify APIs
identify :: AuthClientM NoContent

-- | Rhythm-related APIs
getRhythmConnectedStatus :: AuthClientM LeafBinaryState
getRhythmActiveStatus    :: AuthClientM LeafBinaryState
getRhythmId              :: AuthClientM Int
getRhythmHwVersion       :: AuthClientM T.Text
getRhythmFwVersion       :: AuthClientM T.Text
getRhythmAuxStatus       :: AuthClientM LeafBinaryState
getRhythmAuxMode         :: AuthClientM LeafRhythmMode
getRhythmPosition        :: AuthClientM LeafRhythmPosition
setRhythm                :: LeafSetRhythm -> AuthClientM NoContent

type Token = T.Text

newtype AuthClientM a = AuthClientM
  { unwrapAuthClient :: ReaderT (LeafURL, Token) IO (Either ClientError a) }

-- | Authenticated APIs
-- | Get panel information
getPanelInfo
  -- | Color-related APIs
  :<|> getPowerState
  :<|> getBrightnessState
  :<|> getHueState
  :<|> getSaturationState
  :<|> getColorTempState
  :<|> getColorModeState
  :<|> setState
  -- | Effects
  :<|> getSelectedEffect
  :<|> getAvailableEffects
  :<|> setEffect
  -- | Panel Layout APIs
  :<|> getGlobalOrientation
  :<|> getPanelsLayout
  :<|> setLayout
  -- | Identify APIs
  :<|> identify
  -- | External Control (Streaming): See Effects
  -- | Rhythm
  :<|> getRhythmConnectedStatus
  :<|> getRhythmActiveStatus
  :<|> getRhythmId
  :<|> getRhythmHwVersion
  :<|> getRhythmFwVersion
  :<|> getRhythmAuxStatus
  :<|> getRhythmAuxMode
  :<|> getRhythmPosition
  :<|> setRhythm
  = hoistClient api nt (client api)
  where api = Proxy @AuthenticatedAPI
        nt ep = AuthClientM $ do
          (leafUrl, token) <- ask
          let baseUrl = leafUrl { baseUrlPath = path }
              path = baseUrlPath leafUrl ++ "/api/v1/" ++ (T.unpack token)
          clientMgr <- liftIO $ newManager defaultManagerSettings
          liftIO $ runClientM ep (mkClientEnv clientMgr baseUrl)
