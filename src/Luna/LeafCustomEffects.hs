{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}

module Luna.LeafCustomEffects where

import Control.Monad (replicateM)
import Data.Aeson
import Data.Proxy
import GHC.Generics (Generic)
import GHC.TypeLits (KnownNat, natVal)
import GHC.TypeNats (Nat)
import Luna.LeafClient
import Test.QuickCheck (Arbitrary(..), choose)

data PaneFrame (paneId :: Nat) = PaneFrame { r, g, b, time :: Int}
  deriving (Eq, Generic)

instance Arbitrary (PaneFrame id) where
  arbitrary = PaneFrame <$> e <*> e <*> e <*> choose (5, 10)
    where e = choose (0, 255)

instance KnownNat pn => Show (PaneFrame pn) where
  show PaneFrame{..} = "Frame of pane " <> show (natVal @pn Proxy)
    <> " (" <> show r <> "," <> show g <> "," <> show b <> ") "
    <> show (fromIntegral time / 10) <> "s"

class HasPaneId a where
  getPaneId :: a -> Integer

instance KnownNat pn => HasPaneId (PaneFrame pn) where
  getPaneId _ = natVal (Proxy @pn)

data PaneFrames = forall pId.
  ( KnownNat pId
  , Eq (PaneFrame pId)
  , Show (PaneFrame pId)
  ) => PaneFrames [PaneFrame pId]

instance Arbitrary PaneFrames where
  arbitrary = choose (1, 300) >>=
    \n -> PaneFrames <$> replicateM n (arbitrary @(PaneFrame 1))

deriving instance Show PaneFrames

newtype PaneAnimation = PaneAnimation [PaneFrames]
  deriving Show
  deriving newtype Semigroup

instance Arbitrary PaneAnimation where
  arbitrary = choose (1, 10) >>= \n -> PaneAnimation <$> replicateM n arbitrary

class LeafEffectEncode a where
  encodeEffect :: a -> String

instance LeafEffectEncode (PaneFrame n) where
  encodeEffect PaneFrame{..} =
    unwords (show <$> [r, g, b, 0 {- ignored value -}, time])

instance LeafEffectEncode PaneAnimation where
  encodeEffect (PaneAnimation frames) =
    unwords . ([show $ length frames] <>) . concat $ encFrames <$> frames
    where encFrames (PaneFrames fs) = fields <> fmap encodeEffect fs
            where fields = [show paneId, show $ length fs]
                  paneId = getPaneId (head fs)

showPaneAnimationCmd :: PaneAnimation -> LeafEffectSetState
showPaneAnimationCmd anim = LeafEffectSetState Nothing (Just cmd)
  where cmd :: LeafEffectCmdObj
        cmd = (\case (Success s) -> s) . fromJSON $ object
              [ ("command", "display")
              , ("version", "1.0")
              , ("animType", "custom")
              , ("animData", toJSON (encodeEffect anim))
              , ("loop", toJSON True)
              ]
