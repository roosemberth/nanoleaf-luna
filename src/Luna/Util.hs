module Luna.Util where

import Data.Char

lowerHead :: [Char] -> [Char]
lowerHead (a:as) = toLower a : as
