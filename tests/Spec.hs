{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

import Data.Aeson
import Data.Aeson.QQ
import Luna.LeafClient
import Luna.LeafCustomEffects
import Test.QuickCheck

-- | Sample borrowed from section 5.4.3 (write). Slightly modified for aesonQQ.
sampleLeafEffectCmdObj :: Value
sampleLeafEffectCmdObj = [aesonQQ|
  { animName: "Northern Lights"
  , loop: true
  , palette:
    [ {hue:227,saturation:100,brightness:99}
    , {hue:182,saturation:100,brightness:100}
    , {hue:125,saturation:100,brightness:93}
    , {hue:62,saturation:100,brightness:100}
    , {hue:31,saturation:100,brightness:100}
    , {hue:2,saturation:100,brightness:100}
    , {hue:307,saturation:100,brightness:100}
    ]
  , transTime: {maxValue:20,minValue:20}
  , windowSize: 2
  , flowFactor: 0
  , delayTime: {maxValue:20,minValue:5}
  , colorType: "HSB"
  , animType: "wheel"
  , explodeFactor: 0
  , brightnessRange: {maxValue:0,minValue:0}
  , direction: "left"
  }|]

prop_deserializeSameLeafEffectCmdObj :: Property
prop_deserializeSameLeafEffectCmdObj = case res of
  Data.Aeson.Error str -> counterexample str (property False)
  Data.Aeson.Success a -> property True
  where res :: Data.Aeson.Result LeafEffectCmdObj
        res = fromJSON sampleLeafEffectCmdObj

sampleCustomStaticDisplay :: Value
sampleCustomStaticDisplay = [aesonQQ|
  { command: "display"
  , animType: "static"
  , animData: "3 82 1 255 0 255 0 20 60 1 0 255 255 0 20 118 1 0 0 0 0 20"
  , loop: false
  }|]

prop_encodeSamePanelAnimation = property $
  sampleAnimationData === encodeEffect (PaneAnimation [f1, f2, f3])
  where f1 = PaneFrames [PaneFrame @82  255 0   255 20]
        f2 = PaneFrames [PaneFrame @60  0   255 255 20]
        f3 = PaneFrames [PaneFrame @118 0   0   0   20]
        sampleAnimationData =
          "3 82 1 255 0 255 0 20 60 1 0 255 255 0 20 118 1 0 0 0 0 20"

-- needed on GHC 7.8 and later; without it, quickCheckAll will not be able to find any of the properties.
return []
main = $quickCheckAll
